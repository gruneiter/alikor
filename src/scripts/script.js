import sh from '../blocks/show-hide/show-hide';
import headerNavigation from '../blocks/header-navigation/header-navigation';
import asideMenu from '../blocks/aside-menu/aside-menu';
import tabsMenu from '../blocks/tabs-menu/tabs-menu';
import orderBlock from '../blocks/order-block/order-block';
import promoSlider from '../blocks/promo-slider/promo-slider';
import catalogOpener from '../blocks/catalog-opener/catalog-opener';
import catalogFilter from '../blocks/catalog-filter/catalog-filter';
import register from '../blocks/register/register';
import cartItem from '../blocks/cart-item/cart-item';
import cartTotal from '../blocks/cart-total/cart-total';
import productImages from '../blocks/product-images/product-images';
import sliderSimilarProducts from '../blocks/slider-similar-products/slider-similar-products';
import Tip from '../blocks/tip/tip';

sh();
headerNavigation();
asideMenu();
tabsMenu();
orderBlock();
promoSlider();
catalogOpener();
catalogFilter();
register();
cartItem();
cartTotal();

productImages();
sliderSimilarProducts();

const tips = Array.from(document.querySelectorAll('.tip'));
tips.forEach((item) => {
  const tip = new Tip(item, '.tip__link', '.tip__body', 'tip--active');
  tip.init();
});

const inStockSwitch = document.querySelector('.js-filter-in-stock');
inStockSwitch.addEventListener('change', () => {
  window.dispatchEvent(new CustomEvent('setInStock', { detail: { state: inStockSwitch.checked } }));
});
