var stickyCols = [];
var slider = false;
function refreshSticky() {
  var stickyElements = document.getElementsByClassName('js-sticky-col');
  for (var i = 0; i < stickyCols.length; i++) {

    //stickySidebar.updateSticky();
    stickyCols[i].destroy();
    stickyCols[i] = new StickySidebar(stickyElements[i], {
      topSpacing: 80,
      resizeSensor: true
    });
  }
}

/*
function openProduct(url, backendURL) {
    history.scrollRestoration = 'manual';
    if ($(this).hasClass('js-open-product-popup')) {
        closeDetail();
    }

    $('.page-preloader').addClass('pre-loading');

    $.ajax({
        url: backendURL
    }).done(function (data) {
        //setTimeout(function () {

			$('#product-ajax-container').html(data);

            productDetail();

            backUrl = location.href;
            history.pushState({}, '', url);
            $(window).on('popstate', function (e) {
                $(this).off('popstate');
                closeDetail();
            })

        //}, 1000);
    });
}
*/

function getUrl(name){
  if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
    return decodeURIComponent(name[1]);
}

function openProduct(url, backendURL) {

  history.scrollRestoration = 'manual';
  if ($(this).hasClass('js-open-product-popup')) {
    closeDetail();
  }

  var baseStore = '';
  baseStore = getUrl('base');
  if(baseStore){
    if (url.indexOf('&base') == -1)
      url = url + '&base='+baseStore;
    //backendURL = backendURL + '&baseInfo='+baseStore;
  }

  // console.log(125, backendURL, baseStore);
  productDetail('blank');

  //$('.page-preloader').addClass('pre-loading');
  $.ajax({
    url: backendURL,
    data: {base:baseStore},
  }).done(function (data) {
    console.log(126);
    if($('html').hasClass('detail-product-opened')) {
      console.log(127);
      $('html').removeClass('loading');


      if (data.indexOf('<font class="errortext">Элемент не найден</font>') != -1) {
        $('#product-ajax-container').html('<div class="product-detail-cont" style="padding:20px;"><div class="container page-cont">'+
          '<div class="error-page">'+
          '<img '+
          'src="/local/templates/silver/img/404.jpg"' +
          'srcset="/local/templates/silver/img/404.jpg 1x, /local/templates/silver/img/404@2x.jpg 2x"' +
          'width="700" height="387" alt="" class="error-page__pic">'+
          '<strong class="error-page__title">ошибка</strong>'+
          '<p>К сожалению данной страницы не существует</p>'+
          '<a href="/" class="btn-type2 btn-type2_big"><span class="btn-type2__text">Перейти в каталог</span></a>'+
          '</div></div></div>');
      } else {
        console.log(128);
        $('#product-ajax-container').html(data);

        $('#product-ajax-container .product-detail-cont').append('<div class="load-later_loading js-load-later_loading pre-loading"><div class="loader-logo"><span class="loader-logo__line"></span><span class="loader-logo__line"></span><span class="loader-logo__line"></span><span class="loader-logo__line"></span><span class="loader-logo__line"></span><img src="/local/templates/silver/img/loader-logo-new.svg" width="61" height="46" alt="Алькор"></div></div>');




      }

      productDetail(url);

      backUrl = location.href;
      history.pushState({}, '', url);
      $(window).on('popstate', function (e) {
        $(this).off('popstate');
        closeDetail();
      })

      $(document).ready(function () {
        var body = $(".product-detail-popup");
        console.log(body.scrollTop());
        body.animate({scrollTop:0}, 800, 'swing', function() {
        });
      });

      $('.product-detail-popup__close').click(function() {
        $(this).off('popstate');
        closeDetail();
      });
      //setTimeout(function () {
      //	   productDetail();
//
      //	   backUrl = location.href;
      //	   history.pushState({}, '', url);
      //	   $(window).on('popstate', function (e) {
      //		   $(this).off('popstate');
      //		   closeDetail();
      //	   })
//
      //}, 100);
      // makeSlider();
      $('#videobg,.close244').click(function() {
        videoHide();
      });
    }
  });
  window.dispatchEvent(new Event('productPageLoaded'));
}

var backUrl = null;

$(document).ready(function () {

  /*форма в ЛК менеджера*/
  $(".js-manager-form form").submit(function (e) {
    var arGifts=[];
    if($(".js-gifts-checkbox:checked").length<=0){
      $(".js-gifts-error").show();
      e.preventDefault();
      return false;
    }
    else{
      $(".js-gifts-checkbox:checked").each(function () {
        arGifts.push($(this).val());
      });
      $(".js-gifts-input").val(arGifts.join(", "));
    }

  });
  $(".js-gifts-checkbox").change(function () {
    $(".js-gifts-error").hide();
  });
  $(document).on("change", "select.js-ajax-select", function () {
    var data={};
    if($(this).hasClass("js-metal")){
      data.TYPE="MANAGERS"; //это кого мы будем запрашивать
    }
    if($(this).hasClass("js-manager")){
      data.TYPE="CLIENTS";
      data.MANAGER=$(this).val();
    }
    data.METAL=$("select.js-metal").val();
    $.ajax({
      type: 'POST',
      url: '/local/ajax/managerFormProceed.php',
      dataType: "html",
      data: data,
      success: function (response) {
        if(data.TYPE=="CLIENTS"){
          $("select.js-client").html(response)
            .trigger("refresh")
            .closest(".select-overlay").removeClass("has-overlay");
        }
        if(data.TYPE=="MANAGERS"){
          $("select.js-client").html('<option value="">Нет</option>')
            .trigger("refresh")
            .closest(".select-overlay").addClass("has-overlay");
          $("select.js-manager").html(response)
            .trigger("refresh")
            .closest(".select-overlay").removeClass("has-overlay");
        }
        $(".js-new-client-flag").val("N");
        $(".js-new-client").remove();
      }
    });
  });
  $(document).on("change", "select.js-client", function () {
    if($(this).val()=="new_client"){
      var input='<div style="margin-top:20px;" class="js-new-client new-client"><input type="test" placeholder="Введите название клиента" id="NEW_CLIENT" required="" class="inputtext text-input2" name="'+$(this).attr("name")+'" value="" size="0" /></div>';
      $(this).closest(".form-group").append(input);
      $(".js-new-client-flag").val("Y");
    }
    else{
      $(this).closest(".form-group").find(".js-new-client").remove();
      $(".js-new-client-flag").val("N");
    }
  });
  /***/
  /*форма опроса по выставке*/
  $(".js-meet-service-expert select").change(function () {
    if(parseInt($(this).val())==82){
      $(".js-poll-raiting").addClass("hidden").find("input").prop("disabled", true);
    }
    else{
      $(".js-poll-raiting").removeClass("hidden").find("input").prop("disabled", false);
    }
  });
  /***/

  var stickyElements = document.getElementsByClassName('js-sticky-col');

  for (var i = 0; i < stickyElements.length; i++) {

    stickyCols[i] = new StickySidebar(stickyElements[i], {
      topSpacing: 80,
      resizeSensor: true
    });
  }

  var filterSliders = document.querySelectorAll('.number-range');

  var filterResBlocks = $('.range-slider');

  $('.range-slider__input, .js-number-input').keydown(function (event) {
    if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39)) {
      return;
    } else {
      if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
        event.preventDefault();
      }
    }
  });

  if (filterSliders.length > 0) {

    $('.js-range-slider__input_min').on('change', function () {
      var currentVal = parseInt($(this).val().toString().replace(/[^\/\d]/g, ''));
      var currentId = $(this).attr('data-id');
      filterSliders[currentId].noUiSlider.set([currentVal, null]);
      addValues();
    });

    $('.js-range-slider__input_max').on('change', function () {
      var currentVal = parseInt($(this).val().toString().replace(/[^\/\d]/g, ''));
      var currentId = $(this).attr('data-id');
      filterSliders[currentId].noUiSlider.set([null, currentVal]);
      addValues();
    });

    function addValues()
    {
      for (var i = 0; i < filterSliders.length; i++)
      {
        var currentVal = filterSliders[i].noUiSlider.get();

        if (currentVal[0] != currentVal[1])
        {
          $(filterResBlocks[i]).find('.range-slider__input_min').val(currentVal[0]);
          $(filterResBlocks[i]).find('.range-slider__input_max').val(currentVal[1]);
        }
      }
    }

    var uiSlider = {
      init: function () {
        var rangeInputs = document.querySelectorAll('.number-range');
        uiSlider.setup(rangeInputs);
      },
      setup: function (sliders) {
        function data(element, value) {
          return parseFloat(element.parentElement.getAttribute('data-' + value));
        }
        Array.prototype.forEach.call(sliders, function (slider) {
          var min = data(slider, 'min'),
            max = data(slider, 'max'),
            step = data(slider, 'step'),
            value = slider.parentElement.getAttribute('data-start').split(',');

          noUiSlider.create(slider, {
            start: value,
            connect: true,
            //step: !isNaN(step) ? step : 1,
            step: 0,
            range: {'min': min, 'max': max},
            format: {
              to: function (value) {
                value = parseInt(value);
                return numberFormat(value.toString().replace(/[^\/\d]/g, ''));
                //return value.toString().replace(/[^\/\d]/g,'');
              },
              from: function (value) {
                return value;
              }
            }
          });

          slider.noUiSlider.on('change', function (values) {
            if (window.smartFilter) {
              $(slider.closest('.filter-row')).find('.range-slider__input').trigger('change');
            }
          });

          slider.noUiSlider.on('slide', addValues);
        });
      }
    };

    //uiSlider.init();
    //window.uiSlider = uiSlider;
  }

  if ($('.js-vendor-autocomplete').length > 0) {

    $('.js-vendor-autocomplete').autocomplete({
      source: function (request, response) {
        var catalog = $('.js-vendor-autocomplete').attr('data-catalog');
        $.ajax({
          url: "/local/ajax/search.php?query=" + request.term + '&catalog=' + catalog,
          dataType: "json",
          success: function (data) {
            if (data != null) {
              response($.map(data, function (item) {
                return {
                  label: item.name,
                  value: item.name,
                  link: item.link,
                  picsrc: item.picsrc,
                  category: item.category,
                  detail_page_url: item.detail_page_url
                };
              }));
            }
          }
        })
      }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {

      var inner_html = '<a class="autocomplete-item__link js-open-product" href="' + item.link + '" data-url="' + item.detail_page_url + '"><span class="autocomplete-item__pic"><img src="' + item.picsrc + '" width="" height="" alt=""></span><span class="autocomplete-item__name">' + item.label + '</span><span class="autocomplete-item__category">' + item.category + '</span></a>';

      return $("<li class='autocomplete-item'></li>")
        .data("item.autocomplete", item)
        .append(inner_html)
        .appendTo(ul);
    };
  }

  $(document).on('click', '.js-toggle-filter-row', function () {
    $(this).next('.filter-row__cont').slideToggle(function () {
      refreshSticky();
    });

    $(this).toggleClass('closed');
  });

  $(document).on('click', '.js-hide-all-filters', function () {
    var hideText = $(this).attr('data-hide');
    var showText = $(this).attr('data-show');
    if ($(this).hasClass('opened')) {
      $('.js-toggle-filter-row').addClass('closed');
      $(this).text(hideText);
      $(this).removeClass('opened');
      $('.filter-row__cont').slideUp(function () {
        refreshSticky();
      });
    }
    else {
      $('.js-toggle-filter-row').removeClass('closed');
      $(this).text(showText);
      $(this).addClass('opened');
      $('.filter-row__cont').slideDown(function () {
        refreshSticky();
      });
    }
  });

  $(document).on('click', '.js-filter-more', function () {
    $(this).prev('.checkbox-list').find('.checkbox-list__item_hidden').slideToggle(function () {
      refreshSticky();
    });
    var hideText = $(this).attr('data-hide');
    var showText = $(this).attr('data-show');
    if ($(this).hasClass('opened')) {
      $(this).text(showText);
      $(this).removeClass('opened');
    }
    else {
      $(this).text(hideText);
      $(this).addClass('opened');
    }
  });

  if ($('.detail-pics-scroll').length > 0) {
    productDetail('blank');
    productDetail();
  }

  /*$(document).on("click", function (event) {
      if (!$(event.target).closest(".detail-pics-popup, .js-show-pic-popup, .js-detail-pic-preview, .js-inline-modal-open, .inline-modal, .product-detail-popup__cont, .js-open-product, #bx-panel").length) {
          console.log("TEST_CLICK");
          $('.detail-pics-popup, .inline-modal').removeClass('opened');
          closeDetail();
      }
      if (!$(event.target).closest(".inline-modal, .js-inline-modal-open").length) {
          $('.inline-modal').removeClass('opened');
      }
      if (!$(event.target).closest(".drop-wrap").length) {
          $('.drop-wrap').removeClass('opened');
      }
      event.stopPropagation();
  });*/

  $('body').on('click', '.js-close-product', function (e) {
    closeDetail();
    e.preventDefault();
  });

  // if (window.location.hash) {
  //     loadProductFromHash();
  // }

  $('body').on('click', '.js-open-product,.js-viewed-open,.js-open-product-popup', function (e) {
    var url = $(this).attr('href');
    var backendURL = $(this).attr('data-url');

    openProduct(url, backendURL);
    e.preventDefault();
  });

  $('body').on('click', '.js-inline-modal-open', function (e) {
    var currentModal = $(this).attr('href');
    $('.inline-modal').removeClass('opened');
    $('.drop-wrap').removeClass('opened');
    $(currentModal).addClass('opened');
    e.preventDefault();
  });

  $('body').on('click', '.js-submit-form', function (e) {
    var $currentModal = $(this).closest('.inline-modal');
    if ($(this).closest('form').valid()) {
      $currentModal.addClass('show-ok');
      setTimeout(function () {
        $currentModal.removeClass('show-ok');
        $currentModal.find('form')[0].reset();
      }, 3000);
      return false;
    }
  });


  $('.js-toggle-order-table').on('click', function () {
    $(this).toggleClass('opened');
    for (var i = 0; i < stickyCols.length; i++) {
      //stickyCols[i].reinit();
      stickyCols[i].updateSticky();
    }

  });

  $('.js-show-order-rows').on('click', function () {
    $(this).closest('tr').nextAll('tr').removeClass('order-detail-table__row-hidden');
    $(this).hide();
    refreshSticky()
  });


  $('.js-show-all-sizes').on('click', function (e) {
    var $currentBtn = $(this);
    var $hiddenItems = $currentBtn.closest('.counter-list').find('.counter-list__item_hidden');
    var btnText = $currentBtn.attr('data-show');
    if (!$currentBtn.hasClass('opened')) {
      $hiddenItems.slideDown({
        start: function () {
          $(this).css({
            display: "flex"
          })
        }
      });
      btnText = $currentBtn.attr('data-hide');
      //$hiddenItems.removeClass('counter-list__item_show');
    }
    else {
      $hiddenItems.slideUp();
      //$hiddenItems.addClass('counter-list__item_show');
    }
    $currentBtn.find('.underlined-link__text').text(btnText);
    $currentBtn.toggleClass('opened');
    e.preventDefault();
  });

  $('body').on('change', 'input.js-auto-earrings', function () {
    var sum = 0;

    var $currentEarrings = $(this).closest('.set-item').find('input.js-auto-earrings-val');

    $(this).closest('.counter-list').find('.js-auto-earrings').each(function () {
      if ($(this).val()) {
        var current = parseInt($(this).val());
        var max = parseInt($(this).attr('max'));
        if (current > max) current = max;
        sum += parseInt(current < 0 ? 0 : current);
      }
    });

    if (sum <= parseInt($currentEarrings.attr('max'))) {

      if (sum > 0) {
        $currentEarrings.closest('.jq-number.js-auto-earrings-val').find('.jq-number__spin.minus').removeClass('disabled');
        $currentEarrings.removeClass('empty');
      } else {
        $currentEarrings.closest('.jq-number.js-auto-earrings-val').find('.jq-number__spin.minus').addClass('disabled');
        $currentEarrings.addClass('empty');
      }

      $currentEarrings.val(sum);
      $currentEarrings.closest('.hint').addClass('show');
      //setTimeout(function (e) {
      //    $currentEarrings.closest('.hint').removeClass('show');
      //}, 2000);
    }
  });

  $('body').on('click', '.js-show-stock-table', function () {

    if($(this).closest('.tabs-panes__item').length>0){
      $(this).closest('.tabs-panes__item').find('.stock-table__hidden').toggleClass('opened');
    }
    else{
      $(this).closest('.product-block-item').find('.stock-table__hidden').toggleClass('opened');
    }


    var hideText = $(this).attr('data-hide');
    var showText = $(this).attr('data-show');
    if($(this).hasClass('opened')){
      $(this).text(showText);
      $(this).removeClass('opened');
    }
    else{
      $(this).text(hideText);
      $(this).addClass('opened');
    }
  });


  if($('.js-check-new-filter input').parent().hasClass('disabled')){
    $('.catalog-filter-inline .catalog-filter-inline__item').each(function () {
      if($(this).find('.checkbox__text').text() == 'Новинки'){
        $(this).find('.checkbox').addClass('disabledInfo');
      }
    });
  } else if($('.js-check-new-filter input').parent().length == 0){
    $('.catalog-filter-inline .catalog-filter-inline__item').each(function () {
      if($(this).find('.checkbox__text').text() == 'Новинки'){
        $(this).find('.checkbox').addClass('disabledInfo');
      }
    });
  }
  if($('.js-check-hit-filter input').parent().hasClass('disabled')){
    $('.catalog-filter-inline .catalog-filter-inline__item').each(function () {
      if($(this).find('.checkbox__text').text() == 'Хиты'){
        $(this).find('.checkbox').addClass('disabledInfo');
      }
    });
  } else if($('.js-check-hit-filter input').parent().length == 0){
    $('.catalog-filter-inline .catalog-filter-inline__item').each(function () {
      if($(this).find('.checkbox__text').text() == 'Хиты'){
        $(this).find('.checkbox').addClass('disabledInfo');
      }
    });
  }
  if($('.js-check-spec-filter input').parent().hasClass('disabled')){
    $('.catalog-filter-inline .catalog-filter-inline__item').each(function () {
      if($(this).find('.checkbox__text').text() == 'Акции'){
        $(this).find('.checkbox').addClass('disabledInfo');
      }
    });
  } else if($('.js-check-spec-filter input').parent().length == 0){
    $('.catalog-filter-inline .catalog-filter-inline__item').each(function () {
      if($(this).find('.checkbox__text').text() == 'Акции'){
        $(this).find('.checkbox').addClass('disabledInfo');
      }
    });
  }
});

function showTooltipCount(el)
{
  if (el)
  {
    //var topPos = $(el).closest('.filter-row').position().top + 105;
    $(document).ready(function () {
      if($('.js-check-new-filter input').parent().hasClass('disabled')){
        $('.catalog-filter-inline .catalog-filter-inline__item').each(function () {
          if($(this).find('.checkbox__text').text() == 'Новинки'){
            $(this).find('.checkbox').addClass('disabledInfo');
          }
        });
      } else if ($('.js-check-new-filter input').parent().hasClass('disabled') == false && $('.js-check-new-filter input').parent().length > 0){
        $('.catalog-filter-inline .catalog-filter-inline__item').each(function () {
          if($(this).find('.checkbox__text').text() == 'Новинки'){
            $(this).find('.checkbox').removeClass('disabledInfo');
          }
        });
      } else if($('.js-check-new-filter input').parent().length == 0){
        $('.catalog-filter-inline .catalog-filter-inline__item').each(function () {
          if($(this).find('.checkbox__text').text() == 'Новинки'){
            $(this).find('.checkbox').addClass('disabledInfo');
          }
        });
      }

      if($('.js-check-hit-filter input').parent().hasClass('disabled')){
        $('.catalog-filter-inline .catalog-filter-inline__item').each(function () {
          if($(this).find('.checkbox__text').text() == 'Хиты'){
            $(this).find('.checkbox').addClass('disabledInfo');
          }
        });
      } else if ($('.js-check-hit-filter input').parent().hasClass('disabled') == false && $('.js-check-hit-filter input').parent().length > 0){
        $('.catalog-filter-inline .catalog-filter-inline__item').each(function () {
          if($(this).find('.checkbox__text').text() == 'Хиты'){
            $(this).find('.checkbox').removeClass('disabledInfo');
          }
        });
      } else if($('.js-check-hit-filter input').parent().length == 0){
        $('.catalog-filter-inline .catalog-filter-inline__item').each(function () {
          if($(this).find('.checkbox__text').text() == 'Хиты'){
            $(this).find('.checkbox').addClass('disabledInfo');
          }
        });
      }

      if($('.js-check-spec-filter input').parent().hasClass('disabled')){
        $('.catalog-filter-inline .catalog-filter-inline__item').each(function () {
          if($(this).find('.checkbox__text').text() == 'Акции'){
            $(this).find('.checkbox').addClass('disabledInfo');
          }
        });
      } else if ($('.js-check-spec-filter input').parent().hasClass('disabled') == false && $('.js-check-spec-filter input').parent().length > 0){
        $('.catalog-filter-inline .catalog-filter-inline__item').each(function () {
          if($(this).find('.checkbox__text').text() == 'Акции'){
            $(this).find('.checkbox').removeClass('disabledInfo');
          }
        });
      } else if($('.js-check-spec-filter input').parent().length == 0){
        $('.catalog-filter-inline .catalog-filter-inline__item').each(function () {
          if($(this).find('.checkbox__text').text() == 'Акции'){
            $(this).find('.checkbox').addClass('disabledInfo');
          }
        });
      }

      if($(el).closest('.smartfilter').find('.underlined-link.clear-filter.hideFilter').length > 0 ||
        $(el).closest('.smartfilter').find('.catalog-filter.bx-filter-parameters-box >  .underlined-link.clear-filter + .bx-filter-container-modef').length > 0){
        var topPos = $(el).closest('.checkbox-list__item, .range-slider-input').position().top + 75;
      } else {
        var topPos = $(el).closest('.checkbox-list__item, .range-slider-input').position().top + 105;
      }


      //console.log(topPos);

      if ($(el).closest('.checkbox-list__item.checkbox-list__item_arrow').length > 0 && !$(el).parent('label').hasClass('filter-name-header'))
      {
        topPos += $(el).closest('.checkbox-list__item.checkbox-list__item_arrow').position().top; // checkbox-list__item_arrow - position relative // Dmitry
      }

      if($(el).closest("form").hasClass("smartfilter_no-search")){ //на новых разделах Для выставки в фильтре нет поиска по наименованию
        topPos-=75;
      }
      console.log(123);
      $('.filter-results-label').css('transform', 'translateY('+topPos+'px)').addClass('show');

      var attLink = $('.filter-results-label .js-click-preload').attr('href');
      $('.catalog-filter-inline .catalog-filter-inline__item').each(function () {
        var infoL = $(this).find('.checkbox__input').attr('data-url');
        infoL = infoL.split('?');
        if(infoL.length > 0){
          infoL[0] = attLink;
          $(this).find('.checkbox__input').attr('data-url', infoL.join('?'));
        }
      });
    });
  }
}

/*
function loadProductFromHash() {

    var productCode = window.location.hash.split('-')[1];

    if (productCode) {

        productCode = "/" + productCode;

        var pageURL = queryString.parse(location.search);

        if (pageURL.base) {
            productCode = productCode + '?base=' + pageURL.base;
        }
    }

    $('.page-preloader').addClass('pre-loading');

    $.ajax({
        url: productCode
    }).done(function (data) {
        setTimeout(function () {

            //
            if (data.indexOf('<font class="errortext">Элемент не найден</font>') != -1) {
                $('#product-ajax-container').html('<div class="product-detail-cont" style="padding:20px;">Товар не найден</div>');
            } else {
                $('#product-ajax-container').html(data);
            }
            //

            //$('#product-ajax-container').html(data);

            productDetail();
        }, 1000);
    });
}
*/

$(window).on('load', function () {
  refreshSticky();
});

function catalogLoad() {
  $('.js-small-item-scroll:not(.slick-initialized)').each(function () {
    var $currentSlider = $(this);
    $currentSlider.slick({
      lazyLoad: 'progressive',
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: true
    });

    $currentSlider.on('afterChange', function (event, slick, currentSlide) {
      var $zoomItem = $currentSlider.closest('.item-tile').find('.item-tile__zoom');
      $zoomItem.attr('data-pic-id', currentSlide)
    });

  });

  $('.catalog-list__item').removeClass('animated');
  refreshSticky();

  $('.page-preloader').removeClass('pre-loading');
}


function closeDetail() {
  if (detailOpenedFl) {
    if (backUrl !== null) {
      // console.log(7777, backUrl);
      // backUrl = location.protocol + '//' + location.host + location.pathname;//if it useful somewhere - it should be used only in that case, because we don't need to reset this value on pages like /gold/similar/?id=2836633&PAGEN_2=3 and /gold/?new=1
      var baseStore = '';
      baseStore = getUrl('base');
      if(baseStore){
        baseStore = '?base='+baseStore;
        //backUrl = backUrl + '?base='+baseStore;
      }
      var pageInfo = '';
      pageInfo = getUrl('PAGEN_2');
      if(pageInfo){
        pageInfo = '?PAGEN_2='+pageInfo;
        //backUrl = backUrl + '?base='+baseStore;
      }
      if(pageInfo === null || pageInfo === undefined){
        pageInfo = getUrl('PAGEN_1');
        if(pageInfo){
          pageInfo = '?PAGEN_1='+pageInfo;
          //backUrl = backUrl + '?base='+baseStore;
        }
      }
      // console.log(8888, backUrl);
      //console.log(baseStore);
      if(baseStore !== null && baseStore !== undefined){
        history.pushState({}, '', location.pathname + baseStore);
      } else if (pageInfo !== null && pageInfo !== undefined) {
        history.pushState({}, '', location.pathname + pageInfo);
      } else {
        baseStore = getUrl('item');
        backUrl = backUrl.replace("?item="+baseStore+'&', "?");
        backUrl = backUrl.replace("?item="+baseStore, "");
        history.pushState({}, '', backUrl);
      }
      backUrl = null;
    }
    else {
      history.pushState({}, '', location.pathname);
    }

    detailOpenedFl = false;
    $('html').removeClass('detail-product-opened').css('top', 0);
    setTimeout(function () {
      $('#product-ajax-container').empty();



      refreshSticky()

    }, 400);
    $('html, body').scrollTop(winTop);
  }
}


function countProductHeader() {
  var width = $('.card-header-wrap').width();

  // console.log($('.product-detail-popup__cont').width(), $('.product-detail-popup__cont').outerWidth(), $('.product-detail-popup__cont').innerWidth());

  //  $('.card-header').width(width);
}

function getCatalogType()
{
  var result = 'silver';

  if (location.href.indexOf('/gold/') != -1)
  {
    result = 'gold';
  }

  return result;
}

function productDetail(url)
{
  console.log('show detail ' + url);
  // console.log(331);
  if (url.length==0){
    url='blank';
  }
  if(url == 'blank'){
    // console.log(332);
    // detailOpenedFl = true;

    winTop = $(window).scrollTop();
    // console.log(template, 777);
    $('html').addClass('detail-product-opened loading').css('top', -winTop);

    // $('.page-preloader').addClass('pre-loading');
    // $('.page-preloader').removeClass('pre-loading');

    $('#product-ajax-container').html('<div class="product-detail-cont" style="padding:20px;"><div class="container page-cont">'+
      '<div class="loading-page">'+
      '<div class="page-preloader pre-loading"><div class="loader-logo"><span class="loader-logo__line"></span><span class="loader-logo__line"></span><span class="loader-logo__line"></span><span class="loader-logo__line"></span><span class="loader-logo__line"></span><img src="/local/templates/silver/img/loader-logo-new.svg" width="61" height="46" alt="Алькор"></div></div>'+
      '</div></div></div>');
  }
  else{
    // console.log(333, url);
    detailOpenedFl = true;

    // winTop = $(window).scrollTop();

    // $('html').addClass('detail-product-opened').css('top', -winTop);

    $('.page-preloader').removeClass('pre-loading');

    productPicScroll();

    /*
    var catalogType = getCatalogType();

    if (catalogType == 'silver')
    {
        productPicScroll();
    }
    else
    {
        productPicScrollGold();
    }
    */

    //$('select, input[type="number"]').styler();

    $('select, input[type="number"]').styler(
      {
        onFormStyled: function () {
          $('.jq-number').append('<div class="error"></div>');
        }
      });

    $('input[type="number"]').each(function () {
      numberInput($(this))
    });

    setTimeout(function () {
      countProductHeader();
    }, 100);

    $(window).on('resize load', countProductHeader);


    // const urlParams = new URLSearchParams(url);
    // const item = urlParams.get('item');

    // let code = '/silver/'+$('.product-block-item').attr('data-code');
    let code = $('.product-block-item').attr('data-code');
    // $('.product-block-item').attr('data-code');
    // console.log(code+'.html');
    $.ajax({
      type: 'POST',
      url: code+'.html',
      dataType: "json",
      data: {'load_later': 'Y'},
      error: function (jqXHR, exception) {
        console.log(jqXHR);
        console.log(exception);
      },
      success: function (response) {
        console.log('success');
        console.log('response', response);

        // console.log($('.product-block-item'), '.product-block-item');
        if (response['OK'] == "Y") {
          // console.log('content', response['CONTENT']);
          if (response['CONTENT'] !== null) {
            if (response['CONTENT']['GARNITURES'] !== null) {
              $('.product-block-item[data-code="' + code + '"]').after(response['CONTENT']['GARNITURES']);
              window.dispatchEvent(new Event('productPageGarnitureLoaded'));
            }
            if (response['CONTENT']['SIMILAR'] !== null) {
              $('.product-block-item[data-code="' + code + '"]').append(response['CONTENT']['SIMILAR']);
              window.dispatchEvent(new Event('productPageSimilarLoaded'));
            }
          }
          $('.js-load-later_loading').remove();
          setTimeout(function () {
            countProductHeader();
          }, 100);


          $('.product-block-item-garniture select, .product-block-item-garniture input[type="number"]').styler(
            {
              onFormStyled: function () {
                $('.jq-number').append('<div class="error"></div>');
              }
            });

          $('.product-block-item-garniture input[type="number"]').each(function () {
            numberInput($(this))
          });

          setTimeout(function () {
            countProductHeader();
          }, 100);









          //similar
          $('.js-items-scroll.items-similar').each(function () {
            console.log($(this).hasClass('slick-initialized'));
            $(this).slick({
              slidesToShow: 7,
              slidesToScroll: 3,
              arrows: true,
              dots: false,
              infinite: false,
              prevArrow: '<button type="button" class="slick-prev"><span>&#xe91b;</span></button>',
              nextArrow: '<button type="button" class="slick-next"><span>&#xe91c;</span></button>',
              responsive: [
                {
                  breakpoint: 1280,
                  settings: {
                    slidesToShow: 5
                  }
                },
                {
                  breakpoint: 992,
                  settings: {
                    slidesToShow: 4
                  }
                }
              ]
            });
          });

        }
        // else {
        //     alert(response['MESSAGE']);
        // }
      }
    });



  }

  /* if (!slider && !$('.detail-pics-scroll .fotorama__nav__shaft').hasClass('slick-vertical')) {
       console.log('slider start');
       $('.detail-pics-scroll .fotorama__nav__shaft .fotorama__thumb-border').remove();
       $('.detail-pics-scroll .fotorama__nav__shaft').slick({
           prevArrow: '<div class="slick-prev-custom"></div>',
           nextArrow: '<div class="slick-next-custom"></div>',
           vertical: true,
           verticalSwiping: true,
           slidesToShow: 3,
           autoplay: false,
       });
       slider = true;
   }*/
  console.log('show detail');

}

function modalGalleryInit(picId) {

  var $currentGallery = $('.modal-gallery-scroll');
  var windWidth = viewport().width;
  var modalScrollOpt = {};

  function countScroll() {
    var margin = 30;
    windWidth = viewport().width;

    var scrollWidth = $currentGallery.outerWidth();

    if (windWidth < 992) {
      margin = 20;
    }

    modalScrollOpt = {
      fit: 'scaledown',
      allowfullscreen: false,
      margin: 0,
      arrows: true,
      trackpad: true,
      keyboard: false,
      width: '100%',
      height: scrollWidth,
      loop: true,
      nav: 'thumbs',
      click: true,
      thumbwidth: 70,
      thumbheight: 70,
      thumbfit: 'contain',
      thumbborderwidth: 1,
      thumbmargin: margin,
      startindex: picId
    };
  }

  countScroll();

  $currentGallery.fotorama(modalScrollOpt);

  var modalScrollData = $currentGallery.data('fotorama');

  $(window).on('load resize', function () {
    countScroll();

    modalScrollData.setOptions(modalScrollOpt);
  });

}
