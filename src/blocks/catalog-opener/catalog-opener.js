const catalogOpener = () => {
  const openers = document.querySelectorAll('.catalog-opener');
  openers.forEach((opener) => {
    opener.addEventListener('click', (event) => {
      Array.from(openers).filter((item) => item !== event.target).forEach((item) => item.classList.remove('catalog-opener--active'));
      event.target.classList.toggle('catalog-opener--active');
      const hiddenElement = opener.nextElementSibling;
      if (hiddenElement.classList.contains('catalog-filter')) {
        const root = document.querySelector('html');
        if (event.target.classList.contains('catalog-opener--active')) root.classList.add('no-scroll');
        else root.classList.remove('no-scroll');
      }
    });
  });
};

export default catalogOpener;
