import Slider from '../slider/slider';

const sliderSimilarProductsInit = () => {
  const sliderSections = document.querySelectorAll('.slider-similar-products');
  if (sliderSections.length < 1) return;
  sliderSections.forEach((sliderSection) => {
    const sliderElement = sliderSection.querySelector('.swiper');
    const sliderSettings = {
      slidesPerView: 'auto',
      slidesPerGroup: 1,
      freeMode: true,
    };
    const slider = new Slider(sliderElement, sliderSettings);
    slider.init();
  });
};
const sliderSimilarProducts = () => {
  sliderSimilarProductsInit();
  window.addEventListener('productPageSimilarLoaded', sliderSimilarProductsInit);
  window.addEventListener('productPageGarnitureLoaded', sliderSimilarProductsInit);
};

export default sliderSimilarProducts;
