const register = () => {
  const registerDescription = document.querySelector('.register__description');
  if (!registerDescription) return;
  registerDescription.addEventListener('click', (event) => {
    if (event.target.closest('.register__description-show-link')) {
      event.preventDefault();
      registerDescription.classList.add('register__description--active');
    }
  });
};

export default register;
