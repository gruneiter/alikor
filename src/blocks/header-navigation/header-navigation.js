const headerNavigation = () => {
  const root = document.querySelector('html');
  const nav = document.querySelector('.header-navigation');
  if (!nav) return;
  const switcher = document.querySelector('.header-switcher');
  switcher.addEventListener('click', () => {
    nav.classList.add('header-navigation--active');
    root.classList.add('no-scroll');
  });
  nav.addEventListener('click', (event) => {
    if (event.target.classList.contains('header-navigation__close')) {
      nav.classList.remove('header-navigation--active');
      root.classList.remove('no-scroll');
    }
  });
};

export default headerNavigation;
