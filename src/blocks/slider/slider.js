import {
  Swiper,
  Navigation,
  Pagination,
  EffectFade,
  Thumbs,
} from 'swiper';

class Slider {
  #slider;
  #elem;
  #modules = [Navigation, Pagination, EffectFade, Thumbs];
  #pagination;
  #recievedSettings;
  #paginationSettings = {
    el: '.slider__pagination',
    bulletClass: 'slider__pagination-item',
    bulletActiveClass: 'slider__pagination-item--active',
    clickable: true,
  };

  #settings = {
    modules: this.#modules,
    navigation: {
      nextEl: '.slider__arrow--next',
      prevEl: '.slider__arrow--prev',
      disabledClass: 'slider__arrow--disabled',
    },
  };

  constructor(elem, settings = {}) {
    this.#elem = elem;
    this.#pagination = elem.querySelector('.slider__pagination');
    if (this.#pagination) this.#settings.pagination = this.#paginationSettings;
    this.#recievedSettings = settings;
    Object.assign(this.#settings, this.#recievedSettings);
  }

  get instance() {
    return this.#slider;
  }

  init() {
    this.#slider = new Swiper(this.#elem, this.#settings);
  }
}

export default Slider;
