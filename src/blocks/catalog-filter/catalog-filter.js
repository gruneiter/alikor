const catalogFilter = () => {
  const filter = document.querySelector('.catalog-filter');
  const root = document.querySelector('html');
  if (!filter) return;
  const closeButton = filter.querySelector('.catalog-filter__close');
  const opener = filter.previousElementSibling;
  if (!opener || !closeButton || !opener.classList.contains('catalog-opener')) return;
  closeButton.addEventListener('click', () => {
    root.classList.remove('no-scroll');
    opener.classList.remove('catalog-opener--active');
  });
};

export default catalogFilter;
