export const inStock = () => {
  const inStockSwitch = document.querySelector('.js-filter-in-stock');
  inStockSwitch.addEventListener('change', () => {
    window.dispatchEvent(new CustomEvent('setInStock', { detail: { state: inStockSwitch.checked } }));
  });
};
