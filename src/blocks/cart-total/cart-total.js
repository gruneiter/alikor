const cartTotal = () => {
  const total = document.querySelector('.cart-total');
  if (!total) return;
  total.addEventListener('click', (event) => {
    if (event.target.closest('.cart-total__footer-title')) event.currentTarget.classList.toggle('cart-total--active');
  });
};

export default cartTotal;
