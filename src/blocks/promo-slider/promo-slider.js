import Slider from '../slider/slider';

const promoSlider = () => {
  const sliderSection = document.querySelector('.promo-section');
  if (!sliderSection) return;
  const sliderElement = sliderSection.querySelector('.swiper');
  const sliderSettings = {
    slidesPerView: 1,
    slidesPerGroup: 1,
    effect: 'fade',
    fadeEffect: {
      crossFade: true,
    },
    autoplay: {
      delay: 5000,
    },
  };
  const slider = new Slider(sliderElement, sliderSettings);
  slider.init();
  const sliderClose = sliderSection.querySelector('.promo-section__close');
  if (sliderClose) {
    sliderClose.addEventListener('click', () => {
      const sliderHeight = sliderSection.offsetHeight;
      const sliderMarginTop = parseInt(getComputedStyle(sliderSection).marginTop, 10);
      sliderSection.style.setProperty('--margin-top', `-${sliderHeight - sliderMarginTop}px`);
      sliderSection.classList.add('promo-section--closing');
    });
  }
};

export default promoSlider;
