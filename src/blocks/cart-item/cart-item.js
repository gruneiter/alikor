const cartItem = () => {
  const items = document.querySelectorAll('.cart-item');
  if (!items) return;
  items.forEach((item) => {
    const comment = item.querySelector('.cart-item__comment');
    comment.addEventListener('click', (event) => {
      if (event.target.closest('.cart-item__comment-link')) {
        event.preventDefault();
        event.currentTarget.classList.toggle('cart-item__comment--active');
      }
    });
  });
};

export default cartItem;
