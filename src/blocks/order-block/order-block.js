const orderBlock = () => {
  const blocks = document.querySelectorAll('.order-block');
  blocks.forEach((block) => {
    block.addEventListener('click', (event) => {
      if (event.target.closest('.order-block__header')) block.classList.toggle('order-block--active');
    });
  });
};

export default orderBlock;
