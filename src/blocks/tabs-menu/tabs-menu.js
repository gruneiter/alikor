const tabsMenu = () => {
  const menus = document.querySelectorAll('.tabs-menu');
  if (!menus) return;
  menus.forEach((menu) => {
    const active = menu.querySelector('.tabs-navigation__item--active') || menu.querySelector('.tabs-navigation__item');
    const current = menu.querySelector('.tabs-menu__current');
    if (active) current.innerText = active.innerText;
    current.addEventListener('click', () => menu.classList.toggle('tabs-menu--active'));
  });
};

export default tabsMenu;
