const asideMenu = () => {
  const menu = document.querySelector('.aside-menu');
  if (!menu) return;
  const title = menu.querySelector('.aside-menu__title');
  title.addEventListener('click', () => menu.classList.toggle('aside-menu--active'));
};

export default asideMenu;
