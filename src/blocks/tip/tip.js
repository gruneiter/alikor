export default class Tip {
  constructor(element, linkSelector, bodySelector, activeClass) {
    this.element = element;
    this.linkSelector = linkSelector;
    this.bodySelector = bodySelector;
    this.link = element.querySelector(this.linkSelector);
    this.body = element.querySelector(this.bodySelector);
    this.activeClass = activeClass;
    this.closeTip = (event) => {
      if (
        event.target !== this.link
        && event.target !== this.body
        && this.element.classList.contains(this.activeClass)
      ) this.element.classList.remove(this.activeClass);
      document.removeEventListener('click', this.closeTip);
    };
  }

  init() {
    this.link.addEventListener('click', (e) => {
      e.preventDefault();
      this.element.classList.toggle(this.activeClass);
      if (this.element.classList.contains(this.activeClass)) {
        setTimeout(() => document.addEventListener('click', this.closeTip), 400);
      }
    });
  }
}
