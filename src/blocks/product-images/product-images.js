import Slider from '../slider/slider';

const productImagesInit = () => {
  const sliderSection = document.querySelector('.product-images');
  const sliderThumbsElement = document.querySelector('.product-images-thumbs .swiper');
  if (!sliderSection || !sliderThumbsElement) return;
  const sliderThumbsSettings = {
    slidesPerView: 'auto',
    slidesPerGroup: 1,
    freeMode: true,
    autoHeight: true,
    direction: 'vertical',
    spaceBetween: 10,
    effect: 'slide',
  };
  const sliderThumbs = new Slider(sliderThumbsElement, sliderThumbsSettings);
  sliderThumbs.init();
  const sliderElement = sliderSection.querySelector('.swiper');
  const sliderSettings = {
    slidesPerView: 1,
    slidesPerGroup: 1,
    thumbs: {
      swiper: sliderThumbs.instance,
    },
  };
  const slider = new Slider(sliderElement, sliderSettings);
  slider.init();
};

const productImages = () => {
  productImagesInit();
  window.addEventListener('productPageLoaded', productImagesInit);
  window.addEventListener('productPageGarnitureLoaded', productImagesInit);
};

export default productImages;
